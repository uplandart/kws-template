const fs = require('fs');
const path = require('path');

const env = process.env;
if (!env.NODE_ENV) {
	env.NODE_ENV = 'development';
}
const DEBUG = env.NODE_ENV !== 'production';

let config = {
	env    : env.NODE_ENV,
	debug  : DEBUG,
	port   : env.PORT || 8080,
	pathToRootDir: path.join(__dirname,'..')
};

/**
 * ssl
 */
const ssl = {
	use: env.SSL === 'true' || env.SSL === 't' || false,
};
if (ssl.use) {
	const PATH_TO_SSL_DIR = path.join(__dirname,'..','ssl');
	ssl.key = fs.readFileSync(path.join(PATH_TO_SSL_DIR, env.SSL_KEY || 'ca.key'));
	ssl.crt = fs.readFileSync(path.join(PATH_TO_SSL_DIR, env.SSL_CRT || 'ca.crt'));
}
config.ssl = ssl;

/**
 * session
 */
const session = {
	key   : env.SESSION_KEY     || 'app:sess',
	maxAge: env.SESSION_MAX_AGE || 86400000 /* 1 day */,
};
config.session = session;

/**
 * db
 */
const db = {
	pqsql: {
		main: {
			database: (env.DB_PG_NAME || 'postgres') + (env.NODE_ENV !== 'production' ? '_' + env.NODE_ENV : ''),
			host    : env.DB_PG_HOST || '127.0.0.1',
			port    : env.DB_PG_PORT || 5432,
			user    : env.DB_PG_USER || 'postgres',
			password: env.DB_PG_PASS || 'postgres',
		}
	},
};
config.db = db;

/**
 * jwt
 */
const jwt = {
	secret : env.JWT_SECRET || 'test',
	options: {
		expiresIn: '1d'
	}
};
config.jwt = jwt;

/**
 * mail
 */
const mail = {
	main: {
		host   : process.env.MAIL_HOST     || '127.0.0.1',
		port   : process.env.MAIL_PORT     || 25,
		nick   : process.env.MAIL_NICK     || null,
		user   : process.env.MAIL_USER     || 'test@test.com',
		pass   : process.env.MAIL_PASS     || 'test',
		bccUser: process.env.MAIL_BCC_USER || null,
	},
};
config.mail = mail;

/**
 * web socket
 */
const webSocket = {
	use: env.WS === 'true' || env.WS === 't' || false,
};
if (webSocket.use) {
	webSocket.port = env.WS_PORT || config.port;
}
config.ws = webSocket;

module.exports = config;