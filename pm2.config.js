const OS = require('os');

module.exports = {
	apps : [{
		name            : 'kws-template',
		script          : './index.js',
		watch           : ['server','public','private'],
		ignore_watch    : ['node_modules','src','logs'],

		instance_var    : 'INSTANCE_ID',
		instances       : OS.cpus().length,
		exec_mode       : 'cluster',

		env             : {
			'NODE_ENV': 'development',
		},
		env_production  : {
			'NODE_ENV': 'production'
		},

		merge_logs      : true,
		log_date_format : 'YYYY-MM-DD HH:mm:ss Z'
	}]
};