
exports.up = function(knex, Promise) {
  return Promise.all([
  	knex.schema.createSchema('cls'),
	  knex.schema.withSchema('cls').createTable('actions', (t) => {
	  	t.increments('id').unsigned().primary();
	  	t.string('name', 255).notNull();
	  	t.timestamps(true);
	  }),
	  knex.schema.withSchema('cls').createTable('currencies', (t) => {
	  	t.increments('id').unsigned().primary();
	  	t.string('name', 255).notNull();
	  	t.timestamps(true);
	  }),
	  knex.schema.withSchema('cls').createTable('roles', (t) => {
		  t.increments('id').unsigned().primary();
		  t.string('name', 32).notNull();
		  t.string('description', 255).notNull();
		  t.timestamps(true);
	  }),
	  knex.schema.withSchema('cls').createTable('permission_sections', (t) => {
		  t.increments('id').unsigned().primary();
		  t.string('name', 32).notNull();
		  t.string('description', 255).notNull();
		  t.timestamps(true);
	  }),
	  knex.schema.withSchema('cls').createTable('permissions', (t) => {
		  t.increments('id').unsigned().primary();
		  t.integer('section_id').unsigned().notNull();
		  t.string('name', 32).notNull();
		  t.string('description', 255).notNull();
		  t.timestamps(true);

		  t.foreign('section_id').references('id').inTable('cls.permission_sections');
	  }),

	  knex.schema.createSchema('sec'),
	  knex.schema.withSchema('sec').createTable('role_permissions', (t) => {
		  t.integer('role_id').unsigned().notNull();
		  t.integer('permission_id').unsigned().notNull();
		  t.timestamps(true);

		  t.foreign('role_id').references('id').inTable('cls.roles');
		  t.foreign('permission_id').references('id').inTable('cls.permissions');
	  }),
	  knex.schema.withSchema('sec').createTable('users', (t) => {
	  	t.increments('id').unsigned().primary();
	  	t.integer('role_id').unsigned().notNull();
	  	t.string('login', 255).notNull();
	  	t.string('email', 320).notNull();
	  	t.string('passhash', 64).notNull();
	  	t.boolean('is_del').notNull().defaultTo(false);
	  	t.timestamps(true);

	  	t.foreign('role_id').references('id').inTable('cls.roles');
	  }),

	  knex.schema.createSchema('ent'),
	  knex.schema.withSchema('ent').createTable('wallets', (t) => {
	  	t.increments('id').unsigned().primary();
	  	t.integer('user_id').unsigned().notNull();
	  	t.integer('currency_id').unsigned().notNull();
	  	t.decimal('amount', 15, 6).notNull().defaultTo(0.0);
		  t.boolean('is_del').notNull().defaultTo(false);
	  	t.timestamps(true);

	  	t.foreign('user_id').references('id').inTable('sec.users');
	  	t.foreign('currency_id').references('id').inTable('cls.currencies');
	  }),
	  knex.schema.withSchema('ent').createTable('actions', (t) => {
		  t.increments('id').unsigned().primary();
		  t.integer('from_user_id').unsigned().notNull();
		  t.integer('to_user_id').unsigned().notNull();
		  t.integer('from_wallet_id').unsigned().notNull();
		  t.integer('to_wallet_id').unsigned().notNull();
		  t.integer('action_id').unsigned().notNull();
		  t.integer('currency_id').unsigned().notNull();
		  t.decimal('amount', 15, 6).notNull().defaultTo(0.0);
		  t.timestamp('created_at').notNull().defaultTo(knex.fn.now());

		  t.foreign('from_user_id').references('id').inTable('sec.users');
		  t.foreign('to_user_id').references('id').inTable('sec.users');
		  t.foreign('from_wallet_id').references('id').inTable('ent.wallets');
		  t.foreign('to_wallet_id').references('id').inTable('ent.wallets');
		  t.foreign('action_id').references('id').inTable('cls.actions');
		  t.foreign('currency_id').references('id').inTable('cls.currencies');
	  }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
  	knex.schema.withSchema('ent').dropTable('actions'),
  	knex.schema.withSchema('ent').dropTable('wallets'),
	  knex.schema.dropSchema('ent'),
	  knex.schema.withSchema('sec').dropTable('users'),
	  knex.schema.withSchema('sec').dropTable('role_permissions'),
  	knex.schema.dropSchema('sec'),
	  knex.schema.withSchema('cls').dropTable('permissions'),
	  knex.schema.withSchema('cls').dropTable('permission_sections'),
	  knex.schema.withSchema('cls').dropTable('roles'),
  	knex.schema.withSchema('cls').dropTable('currencies'),
  	knex.schema.withSchema('cls').dropTable('actions'),
	  knex.schema.dropSchema('cls'),
  ]);
};
