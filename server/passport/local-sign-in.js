const { Strategy } = require('passport-local');
const log = require('./../libs/logger')(module);
const dbh = require('./../libs/dbh-instance')();

/**
 * set strategy options
 */
let options = {
	usernameField: 'login',
	passwordField: 'password',
	passReqToCallback: true // allows us to pass back the entire request to the callback
};

/**
 * create local strategy
 */
const strategy = new Strategy(options, (req, usernameFieldData, passwordFieldData, next) => {
	// TODO: check user exist, not blocked, and other ...
	next(null, false);
});

module.exports = strategy;