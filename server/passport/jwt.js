const config = require('config');
const { ExtractJwt, Strategy } = require('passport-jwt');
const log = require('./../libs/logger')(module);
const dbh = require('./../libs/dbh-instance')();

/**
 * set strategy options
 */
let options = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: config.jwt.secret
};

/**
 * create jwt strategy
 */
const strategy = new Strategy(options, (jwtPayload, next) => {
	log.verbose('jwt payload received %j', jwtPayload);

	const expirationDate = new Date(jwtPayload.exp * 1000);
	if (expirationDate < new Date()) {
		log.verbose('jwt is expired');
		return next(null, false);
	}

	dbh(async (db) => {
		//TODO: check user exist, not blocked, and other ...
		return jwtPayload;
	})
		.then(user => {
			next(null, user);
		})
		.catch(err => {
			log.error(err);
			next(err);
		});
});

module.exports = strategy;