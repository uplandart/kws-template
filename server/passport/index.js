const KoaPassport = require('koa-passport');
const strategyLocalSignIn = require('./local-sign-in');
const strategyJwt = require('./jwt');

KoaPassport.use('local-sign-in', strategyLocalSignIn);
KoaPassport.use(strategyJwt);

KoaPassport.serializeUser((user, next) => {
	next(null, user.id);
});

// KoaPassport.deserializeUser((userId, next) => {
// 	next(null, userId);
// });

module.exports = KoaPassport;