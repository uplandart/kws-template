const socketIO = require('socket.io');
const onConnection = require('./ws/connection');

const App = require('koa');
const Server = require('http').createServer(App);

const IO = socketIO(Server);

IO.on('connection', onConnection(IO));

module.exports = { Server, IO };