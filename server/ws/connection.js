const log = require('./../libs/logger')(module);

module.exports = (io) => {
	return (socket) => {
		log.info('New client connected %j', socket.id);

		socket.on('disconnect', (reason) => {
			log.info('Client %j disconnected %j', socket.id, reason);
		});
	}
};