module.exports = (App) => {

	const routerAuth = require('./auth');
	App.use(routerAuth.routes());

	const routerApi = require('./api');
	App.use(routerApi.routes());
	App.use(routerApi.allowedMethods());
};