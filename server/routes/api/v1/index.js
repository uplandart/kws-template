const Router = new require('koa-router')({
	prefix: '/v1'
});
const verifyToken = require('./../../../mw/verify-token');
const {ValidateError} = require('./../../../classes/errors');

Router.get('/', (ctx) => {
	ctx.body = {
		version: process.env.npm_package_version, //'1.0.0',
		current_time: new Date()
	};
});

Router.post('/test-validate-body-verify', (ctx) => {
	const email = ctx.checkBody('email').isEmail().value;

	if (ctx.errors) {
		throw new ValidateError(ctx.errors);
	}

	ctx.body = {done: true, email};
});

Router.get('/test-token-verify', verifyToken(), (ctx) => {
	ctx.body = {done: true, user: ctx.request.user};
});

module.exports = Router;