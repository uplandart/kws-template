const Router = new require('koa-router')({
	prefix: '/api'
});

const routerV1 = require('./v1');
Router.use(routerV1.routes());
Router.use(routerV1.allowedMethods());

module.exports = Router;