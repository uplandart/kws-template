const KoaPassport = require('koa-passport');
const Router = new require('koa-router')({
	prefix: '/auth'
});
const jwt = require('./../libs/jwt');
const log = require('./../libs/logger')(module);
const {ToUserError} = require('./../classes/errors');

Router.post('/sign-in', async (ctx, next) => {
	await KoaPassport.authenticate('local-sign-in', {session: false}, async (err, user, info) => {
		if (err) throw err;
		if (info) throw new ToUserError(info.message || info);
		if (!user) {
			throw new Error('undefined passport authenticate local strategy error');
		}

		try {
			await ctx.logIn(user, {session: false});

			const token = await jwt.signAsync(user);

			ctx.body = {user, token};
		}	catch(e) {
			throw e;
		}
	})(ctx, next);
});

Router.post('/sign-up', async (ctx) => {
	//TODO: create sign up
});

Router.get('/sign-out', (ctx) => {
	const bIsAuthenticated = ctx.isAuthenticated;
	bIsAuthenticated && ctx.logout();
	ctx.body = { done: bIsAuthenticated };
});

module.exports = Router;