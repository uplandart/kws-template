const socketIO = require('socket.io');
const onConnection = require('./ws/connection');

module.exports = (Server) => {
	const IO = socketIO(Server);

	IO.on('connection', onConnection(IO));

	return { Server, IO };
};