const config = require('config');
const Koa = require('koa');
const KoaBodyparser = require('koa-bodyparser');
const KoaValidate = require('koa-validate');
const Passport = require('./passport');
const mwRouteLog = require('./mw/route-log');
const mwErrorHandler = require('./mw/error-handler');
const Routes = require('./routes/index');
const dbhInstance = require('./libs/dbh-instance');
const log = require('./libs/logger')(module);

const App = new Koa();
module.exports = App;

App.use(KoaBodyparser());
KoaValidate(App);
App.use(mwRouteLog);
App.use(Passport.initialize());
// App.use(Passport.session());

App.context.dbh = dbhInstance();

Routes(App);

App.on('error', mwErrorHandler);

process.on('SIGINT', () => {
	App.context.dbh.end()
		.then(() => log.info('db holder is stopped'))
		.catch((err) => log.error(err));
});