const KoaPassport = require('koa-passport');

module.exports = () => {
	return KoaPassport.authenticate('jwt', {session: false});
};