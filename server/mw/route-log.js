const log = require('./../libs/logger')(module);

/**
 * will display in console and save to file at development environment
 */
module.exports = async (ctx, next) => {
	await next();
	log.verbose(`${ctx.req.method} ${ctx.req.url} ${ctx.res.statusCode}`);
};