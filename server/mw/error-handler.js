const config = require('config');
const log = require('../libs/logger')(module);
const {DEFAULT_ERR_MESSAGE, ToUserError, ToUserJsonError, BadAuthenticationError} = require('../classes/errors');

/**
 * middleware will display errors for development mode
 * and write for production mode
 */
module.exports = (err, ctx) => {
	let status, message;

	if (err instanceof BadAuthenticationError) {
		status = err.statusCode;
		message = err.message;
	} else {
		message = (err instanceof ToUserError
				?
				err.message
				:
				(err instanceof ToUserJsonError
						?
						(typeof err.message === 'object' ? err.message : JSON.parse(err.message))
						:
						(config.debug ? err.message : DEFAULT_ERR_MESSAGE)
				)
		);
		status = err instanceof Error ? (err.status ? err.status : 500) : 500;
	}

	log.verbose('catch error and return response %s %j', status, message);
	ctx.status = status;
	ctx.body = message;
};