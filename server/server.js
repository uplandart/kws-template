const http = require('http');
const https = require('https');
const config = require('config');

const App = require('./app');
let Server;

if (config.ssl.use) {
	Server = https.createServer({
		key: config.ssl.key,
		cert: config.ssl.crt
	}, App.callback());
} else {
	Server = http.createServer(App.callback());
}

module.exports = { Server, App };
