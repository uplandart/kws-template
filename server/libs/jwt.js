const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = Object.assign({}, jwt, {
	signAsync,
	verifyAsync
});

function signAsync(payload) {
	return new Promise((resolve, reject) => {
		jwt.sign(payload, config.jwt.secret, config.jwt.options, (err, token) => {
			if (err) return reject(err);
			resolve(token);
		});
	});
}

function verifyAsync(token) {
	return new Promise((resolve, reject) => {
		jwt.verify(token, config.jwt.secret, (err, decoded) => {
			if (err) return reject(err);
			resolve(decoded);
		});
	});
}