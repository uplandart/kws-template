const pg = require('pg');
const pgDbhInit = require('pg-dbh');
const config = require('config');
const log = require('./../libs/logger')(module);

const connect = config.db.pqsql.main;
let params = {
	onError: (err) => {
		log.error(err);
		process.exit(1);
	}
};
params.logger = log.verbose.bind(log);

let dbhInstance;

module.exports = () => {
	if (!dbhInstance) {
		dbhInstance = pgDbhInit(pg, connect, params);
	}
	return dbhInstance;
};