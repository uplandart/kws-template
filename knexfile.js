require('dotenv').config();
const config = require('config');

module.exports = {
	client: 'pg',
	connection: config.db.pqsql.main
};